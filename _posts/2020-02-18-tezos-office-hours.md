---
layout: post
current: post
cover: assets/images/matthew-fournier-m98qq5300rk-unsplash.jpg
navigation: True
title: Tezos Office Hours
excerpt: We are organizing a Tezos meetup at the YAP.cx office on Wednesday, February 26.
date: 2020-02-18 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_fr: /tezos-office-hours/
---

### When?

Wednesday, February 26 from 6:30 PM to 9:30 PM

### Where?

[YAP.cx](https://yap.cx/) office, 201 Notre-Dame St W, Suite 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

Inspired by the recent [Tezos Happy Office Hours](https://www.eventbrite.com/e/tezos-happy-office-hours-tickets-92216218191) meetup, we are organizing a Tezos meetup to hang out and discuss all the recent Tezos developments.

Are you building with Tezos or just curious about it? Come join us at the YAP.cx office to chat with Tezos enthusiasts and builders.

[Simon B.Robert](https://github.com/carte7000), lead developer of [Taquito](https://tezostaquito.io/) (a TypeScript library suite for developing on the Tezos blockchain) will also be present. All of us (Francis, Zak and Simon) will be on hand to answer questions and help people onboard on to Tezos, both as regular users and developers. Bring your laptops if you need help setting up a Tezos wallet or a development environment.

Please note that there won't be any presentations during this meetup but we are planning to organize another meetup in March where Zak and Simon will each do a presentation. We hope that this upcoming meetup will be useful for us to get feedback on what we should talk about in the presentations for the March meetup.

This event is sponsored by the [Tezos Commons Foundation](https://tezoscommons.org/).

### RSVP

- [Decentralized Web Montreal](https://www.meetup.com/decentralized-web-montreal/events/268784797/)
