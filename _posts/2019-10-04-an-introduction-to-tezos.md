---
layout: post
current: post
cover: assets/images/july-10-meetup.jpg
navigation: True
title: An Introduction to Tezos
date: 2019-10-04 12:00:00
tags: [Videos]
class: post-template
subclass: 'post tag-videos'
author: francis
permalink_fr: /une-introduction-a-tezos/
---

Here's a video of the [Tezos presentation](https://docs.google.com/presentation/d/11vQh0UpZx-zBvp4jYqofTu9yahCjHa4YbfyjLUdJsSA/edit?usp=sharing) given by Zakaria during our [July 10 meetup](/tezos-one-year-celebration/).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/mxcUDiSln7Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

We hope it will be a useful resource for the Tezos French-speaking community!
