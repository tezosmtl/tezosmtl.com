---
layout: post
current: post
cover: assets/images/eva-blue-1xgnRBvF_UI-unsplash.jpg
navigation: True
title: Tezos Meetup with Arthur Breitman
excerpt: We are organizing a Tezos meetup at the YAP.cx office this Thursday, September 26.
date: 2019-09-24 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_fr: /meetup-tezos-avec-arthur-breitman/
---

### When?

Thursday, September 26 from 6:30 PM to 8:30 PM

### Where?

[YAP.cx](https://yap.cx/) office, 201 Notre-Dame St W, Suite 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

We are organizing a Tezos meetup at the YAP.cx office this Thursday, September 26. It will be a great opportunity to meet and discuss with other Tezos enthusiasts. If you are new to Tezos, you are welcome to attend this meetup as well, we'll be happy to answer your questions!

[Arthur Breitman](https://twitter.com/arthurb) will be present during the meetup and will do a Q&A and a short presentation.

![Photo Arthur](/assets/images/arthur.jpg)

Arthur Breitman is an early architect of the Tezos protocol. Prior to becoming involved in Tezos full time, he worked at X and Waymo on self-driving cars. In his earlier carreer, he worked as a quantitative analyst at Goldman Sachs and Morgan Stanley. Arthur graduated from the École polytechnique and holds a MS in financial mathematics from the Courant Institute at NYU.

### RSVP

- [Decentralized Web Montreal](https://www.meetup.com/decentralized-web-montreal/events/265119742/)
