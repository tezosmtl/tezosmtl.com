---
layout: post
current: post
cover: assets/images/ray-hennessy-gdTxVSAE5sk-unsplash.jpg
navigation: True
title: Tezos One Year Celebration!
excerpt: To celebrate the one year anniversary of the Tezos network launch, we are organizing a social meetup on Wednesday, July 10.
date: 2019-07-04 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_fr: /celebration-des-un-an-de-tezos/
---

### When?

Wednesday, July 10 from 5:00 PM to 7:00 PM

### Where?

[YAP.cx](https://yap.cx/) office, 201 Notre-Dame St W, Suite 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

[To celebrate the one year anniversary of the Tezos network launch](https://medium.com/tezoscommons/tezos-celebrating-one-year-da53b98a5084), we are organizing a social meetup on Wednesday, July 10.

Zakaria Boukhcheb will give a short introduction to Tezos and liquid-proof-of-stake (15-20 minutes, in French). The rest of the meetup will be mostly networking.

Thanks to [Tezos Commons](https://tezoscommons.org/) for supporting this meetup!

### RSVP

- [Decentralized Web Montreal](https://www.meetup.com/decentralized-web-montreal/events/262874990/)
