---
layout: post
current: post
cover: assets/images/annie-spratt-608001-unsplash.jpg
navigation: True
title: 'Tezos Workshop: How to Delegate XTZ'
excerpt: We are organizing a Tezos workshop at the YAP.cx office on Wednesday, February 20.
date: 2019-02-14 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_fr: /atelier-tezos-deleguer-des-xtz/
---

### When?

Wednesday, February 20 from 6:45 PM to 9:00 PM

### Where?

[YAP.cx](https://yap.cx/) office, 201 Notre-Dame St W, Suite 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

This workshop will be a great opportunity to try out various Tezos wallets that are currently available. We will show you how to use [TezBox](https://tezbox.com), [Galleon](https://galleon-wallet.tech) and [Cortez](https://play.google.com/store/apps/details?id=com.tezcore.cortez). We encourage you to bring a laptop if you'd like to follow our demos as we present them. We will be using test tokens (from the Alphanet and Zeronet networks) so everyone should be able to participate!

We will also show you how to use tools such as [MyTezosBaker](https://mytezosbaker.com/) (to help you choose a baker) and [Baking Bad](https://baking-bad.org/) (to audit your baker).

We are planning to do the workshop in French but we will be happy to repeat our explanations and answer questions in English if not everyone that attends speaks French.

### Slides

Here are the slides of our presentations:

- [Tezos Workshop: How to Delegate XTZ](https://docs.google.com/presentation/d/13EfbBQobIyrfFrEFd1qLheoWDZD-P6mySvSI1Kz2Law/edit?usp=sharing)
- [How to Choose a Baker](https://docs.google.com/presentation/d/1fo1oMJxSlt93gOcSQFz5VH0vp0zcUu-PbjRWm8-9lww/edit?usp=sharing)

Feel free to use them as templates to prepare a presentation on Tezos or simply as a step-by-step guide to delegation.
