---
layout: post
current: post
cover: assets/images/october-20-meetup-tezos-presentation.jpg
navigation: True
title: Tezos Presentation
date: 2018-12-04 12:00:00
tags: [Videos]
class: post-template
subclass: 'post tag-videos'
author: francis
permalink_fr: /presentation-tezos/
---

Here's a video of the [Tezos presentation](https://docs.google.com/presentation/d/1DpJulwkSdTMf_0qaTYPwcpsM5iJRZyhRDPc4ODCUgQg/edit?usp=sharing) given by Zakaria during our [October 20 meetup](/an-introduction-to-proof-of-stake/).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LnxgSz98QY0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

We hope it will be a useful resource for the Tezos French-speaking community!
