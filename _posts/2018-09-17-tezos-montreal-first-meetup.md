---
layout: post
current: post
cover: assets/images/marc-olivier-jodoin-118293-unsplash.jpg
navigation: True
title: Tezos Montreal's First Meetup
excerpt: To celebrate the launch of the Tezos mainnet, we are organizing our first meetup this Wednesday, September 19.
date: 2018-09-17 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_fr: /premier-meetup-de-tezos-montreal/
---

### When?

Wednesday, September 19 from 6:00 PM to 9:00 PM

### Where?

[YAP.cx](https://yap.cx/) office, 201 Notre-Dame St W, Suite 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

We ([Francis Brunelle](https://twitter.com/frabrunelle) and [Zakaria Boukhcheb](https://twitter.com/zakaria_mb)) are happy to announce the creation of the Tezos Montreal meetup group!

This meetup group is for anyone who wants to learn more about [Tezos](https://tezos.com/) and meet other Tezos enthusiasts.

To celebrate the [launch of the Tezos mainnet](https://tezos.foundation/news/tezos-mainnet-is-live), we are organizing our first meetup this Wednesday, September 19.
