---
layout: post
current: post
cover: assets/images/andrew-welch-40121-unsplash.jpg
navigation: True
title: An Introduction to Proof-of-Stake
excerpt: We are organizing a meetup on proof-of-stake at BlockHouse on Saturday, October 20.
date: 2018-10-02 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_fr: /une-introduction-au-proof-of-stake/
---

### When?

Saturday, October 20 from 2:00 PM to 5:00 PM

### Where?

[BlockHouse](https://blockhouse.rcgt.com/en/), 456 De la Gauchetière St W, Suite 200 ([map](https://goo.gl/maps/Aj6Zn9FHBHS2))

### Description

Emmanuel Rochette from [Catallaxy](https://catallaxy.rcgt.com/en/) will present an overview of proof-of-stake. The discussion will start with a short comparison between proof-of-work (PoW) and proof-of-stake (PoS) as a way to explain how the latter came about. It will then follow the evolution of PoS through its uses in different cryptocurrencies. The discussion will end with the attacks that threaten all systems using PoS and their corresponding solutions.

[Zakaria Boukhcheb](https://twitter.com/zakaria_mb) will talk about proof-of-stake in the context of the Tezos blockchain (e.g. explaining how baking works) and compare the implementation of proof-of-stake in Tezos to another popular variant (contrasting [liquid proof-of-stake](https://medium.com/tezos/liquid-proof-of-stake-aec2f7ef1da7) and delegated proof-of-stake). He will also discuss other aspects of Tezos (history, philosophy, governance and potential amendments).

[Samuel Harrison](https://twitter.com/XTZAccelerated) from the [Tezos Commons Foundation](https://tezoscommons.org/) will be giving a presentation on the community and ecosystem of Tezos.

Emmanuel's presentation and Zakaria's presentation will be in French. Samuel's presentation will be in English.

### Slides

Here are the slides of Zakaria's presentation:

- [Tezos Presentation](https://docs.google.com/presentation/d/1DpJulwkSdTMf_0qaTYPwcpsM5iJRZyhRDPc4ODCUgQg/edit?usp=sharing) (in French)
