# tezosmtl.com

> This repository contains the source files for [tezosmtl.com](https://tezosmtl.com/).

## Contributing

By contributing to this website, you dedicate your work to the public domain and relinquish any copyright claims under the terms of the [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

### Submitting a pull request

- [Fork this GitLab repository](https://gitlab.com/tezosmtl/tezosmtl.com/forks/new) and clone your fork locally on your computer.
- Install [Jekyll](https://jekyllrb.com/) and other dependencies with the command `make install`*
- Test out your changes with the command `make serve`
- When you're happy, push your changes up and open a pull request.

\* You should ensure you have [Bundler](https://bundler.io/) installed.

If you're not sure how to open a pull request, feel free to [open an issue](https://gitlab.com/tezosmtl/tezosmtl.com/issues/new) instead.

## Credits

This website is based on a [Jekyll](https://jekyllrb.com/) theme called [Jasper2](https://github.com/jekyller/jasper2) (which itself is a port of Ghost's [Casper](https://github.com/TryGhost/Casper) theme).

## Public domain

Unless otherwise noted, all content on this website is [dedicated to the public domain](https://gitlab.com/tezosmtl/tezosmtl.com/blob/master/LICENSE) under the [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
