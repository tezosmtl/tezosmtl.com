---
layout: page
current: about
title: About Tezos Montreal
navigation: true
class: page-template
subclass: 'post page'
permalink_fr: /a-propos/
---

This meetup group is for anyone who wants to learn more about [Tezos](https://tezos.com/) and meet other Tezos enthusiasts. It was started by [Francis Brunelle](https://twitter.com/frabrunelle) and [Zakaria Boukhcheb](https://twitter.com/zakaria_mb) in September 2018.

To get notified when we announce new meetups, you can [subscribe to our newsletter](#subscribe) or join the [Decentralized Web Montreal](https://www.meetup.com/decentralized-web-montreal/) meetup group.

Please follow us on [Twitter](https://twitter.com/tezosmtl) and join us on [Telegram](https://t.me/tezosmtl) to chat with us and give us feedback.

## Contributing

By contributing to this website, you dedicate your work to the public domain and relinquish any copyright claims under the terms of the [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

### Submitting a pull request

- [Fork this GitLab repository](https://gitlab.com/tezosmtl/tezosmtl.com/forks/new) and clone your fork locally on your computer.
- Install [Jekyll](https://jekyllrb.com/) and other dependencies with the command `make install`*
- Test out your changes with the command `make serve`
- When you're happy, push your changes up and open a pull request.

\* You should ensure you have [Bundler](https://bundler.io/) installed.

If you're not sure how to open a pull request, feel free to [open an issue](https://gitlab.com/tezosmtl/tezosmtl.com/issues/new) instead.

## Credits

This website is based on a [Jekyll](https://jekyllrb.com/) theme called [Jasper2](https://github.com/jekyller/jasper2) (which itself is a port of Ghost's [Casper](https://github.com/TryGhost/Casper) theme).

## Public domain

Unless otherwise noted, all content on this website is [dedicated to the public domain](https://gitlab.com/tezosmtl/tezosmtl.com/blob/master/LICENSE) under the [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
